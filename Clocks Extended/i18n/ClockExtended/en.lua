return {
    name = 'Clock extended',
    description = 'A test plugin to show time and date in a customizable format',
    formatDate = 'Date format (dm/dmy/dmey)',
    formatTime = 'Time format (12h/24h/part)',
    background = 'Widget background (yes/no)',
    posX = 'Widget position X',
    posY = 'Widget position Y',
    sizeX = 'Widget size X',
    sizeY = 'Widget size Y',
}