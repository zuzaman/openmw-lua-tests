local gameSettings = require('openmw.settings')
local input = require('openmw.input')
local util = require('openmw.util')
local ui = require('openmw.ui')
local async = require('openmw.async')
local core = require('openmw.core')
local storage = require('openmw.storage')

local templates = require('scripts.ClockExtended.templates')

local enablePage = ui.registerSettingsPage ~= nil

local L = core.i18n('ClockExtended')
local v2 = util.vector2

local defaultSettings = {
    formatDate = 'dmey',
    formatTime = 'part',
    background = 'yes',
    posX = 0.2,
    posY = 0.0,
    sizeX = 220,
    sizeY = 20,
}

local section = storage.playerSection('ClockExtended')
for k in pairs(defaultSettings) do
    if not section:get(k) then
        section:set(k, defaultSettings[k])
    end
end

if enablePage then
    local LABEL_SIZE = v2(180, 30)
    local PADDING = 3
    local SETTING_OFFSET = v2(LABEL_SIZE.x + PADDING, 0)
    local SETTING_SIZE = v2(150, 30)
    local CONTAINER_SIZE = v2(
        LABEL_SIZE.x + PADDING + SETTING_SIZE.x,
        math.max(LABEL_SIZE.y, SETTING_SIZE.y)
    )

    local function keybindLayout(title, field, update)
        local labelLayout = {
            props = {
                size = LABEL_SIZE,
            },
            content = ui.content {
                {
                    type = ui.TYPE.Text,
                    template = templates.sandText,
                    props = {
                        text = title,
                        relativePosition = v2(0, 0.5),
                        anchor = v2(0, 0.5),
                    },
                },
            },
        }

        local bindLayout = {
            template = templates.box,
            props = {
                position = SETTING_OFFSET,
                size = SETTING_SIZE,
            },
            content = ui.content {
                {
                    name = 'edit',
                    type = ui.TYPE.TextEdit,
                    template = templates.sandText,
                    props = {
                        relativeSize = v2(1, 1),
                        text = input.getKeyName(section:get(field)),
                        textAlignH = ui.ALIGNMENT.Center,
                        textAlignV = ui.ALIGNMENT.Center,
                    },
                    events = {},
                },
            },
        }

        local editWidget = bindLayout.content.edit

        editWidget.events.keyPress = async:callback(function(key)
            section:set(field, key.code)
            editWidget.props.text = input.getKeyName(key.code)
            update()
        end)

        return {
            props = {
                size = CONTAINER_SIZE,
            },
            content = ui.content {
                labelLayout,
                bindLayout,
            },
        }
    end

    local function numberLayout(title, field, update)
        local labelLayout = {
            props = {
                size = LABEL_SIZE,
            },
            content = ui.content {
                {
                    type = ui.TYPE.Text,
                    template = templates.sandText,
                    props = {
                        text = title,
                        relativePosition = v2(0, 0.5),
                        anchor = v2(0, 0.5),
                    },
                },
            },
        }

        local inputLayout = {
            template = templates.box,
            props = {
                position = SETTING_OFFSET,
                size = SETTING_SIZE,
            },
            content = ui.content {
                {
                    name = 'edit',
                    type = ui.TYPE.TextEdit,
                    template = templates.sandText,
                    props = {
                        relativeSize = v2(1, 1),
                        text = tostring(section:get(field)),
                        textAlignH = ui.ALIGNMENT.Start,
                        textAlignV = ui.ALIGNMENT.Center,
                    },
                    events = {},
                },
            },
        }

        local editWidget = inputLayout.content.edit

        editWidget.events.keyPress = async:callback(function(key)
            if key.code == input.KEY.Backspace then
                editWidget.props.text = string.sub(editWidget.props.text, 1, -2)
            elseif key.symbol then
                editWidget.props.text = editWidget.props.text .. key.symbol
            end
            local newValue = tonumber(editWidget.props.text)
            if newValue then
                section:set(field, newValue)
            end
            update()
        end)

        return {
            props = {
                size = CONTAINER_SIZE,
            },
            content = ui.content {
                labelLayout,
                inputLayout,
            },
        }
    end

    local function stringLayout(title, field, update)
        local labelLayout = {
            props = {
                size = LABEL_SIZE,
            },
            content = ui.content {
                {
                    type = ui.TYPE.Text,
                    template = templates.sandText,
                    props = {
                        text = title,
                        relativePosition = v2(0, 0.5),
                        anchor = v2(0, 0.5),
                    },
                },
            },
        }

        local inputLayout = {
            template = templates.box,
            props = {
                position = SETTING_OFFSET,
                size = SETTING_SIZE,
            },
            content = ui.content {
                {
                    name = 'edit',
                    type = ui.TYPE.TextEdit,
                    template = templates.sandText,
                    props = {
                        relativeSize = v2(1, 1),
                        text = tostring(section:get(field)),
                        textAlignH = ui.ALIGNMENT.Start,
                        textAlignV = ui.ALIGNMENT.Center,
                    },
                    events = {},
                },
            },
        }

        local editWidget = inputLayout.content.edit

        editWidget.events.keyPress = async:callback(function(key)
            if key.code == input.KEY.Backspace then
                editWidget.props.text = string.sub(editWidget.props.text, 1, -2)
            elseif key.symbol then
                editWidget.props.text = editWidget.props.text .. key.symbol
            end
            local newValue = tostring(editWidget.props.text)
            if newValue then
                section:set(field, newValue)
            end
            update()
        end)

        return {
            props = {
                size = CONTAINER_SIZE,
            },
            content = ui.content {
                labelLayout,
                inputLayout,
            },
        }
    end

    local element = nil
    local function update()
        if element then
            element:update()
        end
    end
    local settingsList = {
        stringLayout(L('formatDate'), 'formatDate', update),
        stringLayout(L('formatTime'), 'formatTime', update),
        stringLayout(L('background'), 'background', update),
        numberLayout(L('posX'), 'posX', update),
        numberLayout(L('posY'), 'posY', update),
        numberLayout(L('sizeX'), 'sizeX', update),
        numberLayout(L('sizeY'), 'sizeY', update),
    }
    local offset = PADDING
    for _, layout in ipairs(settingsList) do
        layout.props.position = v2(0, offset)
        offset = offset + layout.props.size.y + PADDING
    end
    local settingsLayout = {
        props = {
            position = v2(PADDING, PADDING),
            size = v2(CONTAINER_SIZE.x, offset - PADDING),
        },
        content = ui.content(settingsList),
    }
    element = ui.create {
        props = {
            size = settingsLayout.props.size + v2(2 * PADDING, 2 * PADDING),
        },
        content = ui.content { settingsLayout },
    }
    ui.registerSettingsPage{
        name = L('name'),
        description = L('description'),
        element = element,
    }
end

return section