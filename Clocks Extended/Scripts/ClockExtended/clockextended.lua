local input = require('openmw.input')
local time = require('openmw_aux.time')
local settings = require('scripts.ClockExtended.settings')
local templates = require('scripts.ClockExtended.templates')
local elements = require('scripts.ClockExtended.elements')
local queries = require('scripts.ClockExtended.queries')

local formatTime = nil
local formatDate = nil
local posX = nil
local posY = nil
local sizeX = nil
local sizeY = nil
local background = nil
local changeWidget = false
local clockLayout = nil
local clockElement = nil
local clockTextWidget = nil
local clockContent = nil
local timer = nil


-- this function checks for updates in the script config settings
local function getSettings()
    -- possible changes in the format of the time/date display
    formatTime = tostring(settings:get('formatTime'))
    formatDate = tostring(settings:get('formatDate'))
    
    -- possible changes in the widget dimensions or placing
    newPosX = tonumber(settings:get('posX'))
    newPosY = tonumber(settings:get('posY'))
    newSizeX = tonumber(settings:get('sizeX'))
    newSizeY = tonumber(settings:get('sizeY'))
    newBackground = tostring(settings:get('background'))
    
    -- check if the widget dimension or placing changed
    if newPosX ~= posX or newPosY ~= posY or newSizeX ~= sizeX or newSizeY ~= sizeY or newBackground ~= background then
      changeWidget = true
      posX = newPosX
      posY = newPosY
      sizeX = newSizeX
      sizeY = newSizeY
      background = newBackground
    end
end


-- this function will be the once ran repeatedly, get current script settings and update clock text
local function updateTime()
  if not clockElement then return end
  getSettings()
  clockTextWidget.props.text = queries.getClockContent(formatTime, formatDate)
  
  clockElement:update()
end

-- run the updateTime function repeatedly
local function startUpdating()
  timer = time.runRepeatedly(updateTime, 1 * time.minute, { type = time.GameTime })
end

-- stop running the update repeatedly
local function stopUpdating()
  if timer then
    timer()
    timer = nil
  end
end


-- initial setup function triggering first script config check, creation of widget and start of the repeated timed function
local function initialize(initial)
  if initial == 1 then
    getSettings()
    if background:lower() == 'yes' then
      clockLayout = elements.createLayout(posX, posY, sizeX, sizeY, templates.clockWindow, templates.clockText, queries.getClockContent(formatTime, formatDate))
    else
      clockLayout = elements.createLayout(posX, posY, sizeX, sizeY, templates.clockWindowNoBackground, templates.clockText, queries.getClockContent(formatTime, formatDate))
    end
  end
  if clockLayout ~= nil then
    clockElement, clockTextWidget = elements.createElement(clockLayout)

    startUpdating()
  end
end


-- this is where the script starts
initialize(1)

return {
  engineHandlers = {
    -- activate or deactivate the display with they 'o' key
    onKeyPress = function(key)
      if key.code == input.KEY.O then
        if clockElement then
          clockElement:destroy()
          clockElement = nil
          stopUpdating()
        else
          initialize(1)
        end
      end
    end,
    -- check if the widget dimensions, positioning or background has changed and re-initialize it
    onUpdate = function(key)
      if changeWidget then
        stopUpdating()
        if clockElement then
          clockElement:destroy()
          clockElement = nil
        end
        if background:lower() == 'yes' then
          clockLayout = elements.createLayout(posX, posY, sizeX, sizeY, templates.clockWindow, templates.clockText, queries.getClockContent(formatTime, formatDate))
        else
          clockLayout = elements.createLayout(posX, posY, sizeX, sizeY, templates.clockWindowNoBackground, templates.clockText, queries.getClockContent(formatTime, formatDate))
        end
        changeWidget = false
        if clockLayout ~= nil then
          clockElement, clockTextWidget = elements.createElement(clockLayout)
          startUpdating()
        end
      end
    end
  }
}
