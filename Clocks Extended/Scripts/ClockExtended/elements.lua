local ui = require('openmw.ui')
local async = require('openmw.async')
local util = require('openmw.util')



local v2 = util.vector2

-- create the clock widget layout based on position and size parameters 
local function createLayout(posX, posY, sizeX, sizeY, templateWindow, templateContent, content)
        newLayout = {
        layer = 'Windows',
        type = ui.TYPE.Window,
        template = templateWindow,
        props = {
          position = v2(0, 0),
          relativePosition = v2(posX, posY),
          anchor = v2(1, 0),
          size = v2(sizeX, sizeY),
        },
        events = {
          windowDrag = async:callback(function(coord, layout)
            local p = layout.props
            -- keep user's changes to window position
            p.position = coord.position
            p.anchor = nil
            p.relativePosition = nil
          end),
        },
        content = ui.content {
          {
            type = ui.TYPE.Text,
            name = "text",
            template = templateContent,
            props = {
              relativePosition = v2(0.5, 0.5),
              anchor = v2(0.5, 0.5),
              text = content,
            },
          },
        }
      }
      
      return newLayout
end
      

-- create the clock element based on the layout and its text      
local function createElement(layout)
    element = ui.create(layout)
    textWidget = layout.content.text
    return element, textWidget
end

return {
      createLayout = createLayout,
      createElement = createElement
}

