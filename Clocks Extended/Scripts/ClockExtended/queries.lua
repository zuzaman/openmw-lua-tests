local calendar = require('openmw_aux.calendar')


-- this function queries the calendar and returns data in required time and date format
local function getClockContent(formatTime, formatDate)
  -- date format for displaying day month and year without era
  if formatDate == 'dmy' then
    queryDate = '%d %B %Y'
  -- date format for diaplying day and month
  elseif formatDate == 'dm' then
    queryDate = '%d %B'
  -- default date format dmey for diaplying day, month and year with the era
  else
    queryDate = '%d %B 3E%Y'
  end
  
  -- time format in words, the splitting of the day based on hours is quite arbitrary
  if formatTime:lower() == 'part' then
    -- did not want to mess around with string perations, just get the hour of day
    stringTime = calendar.formatGameTime('%H')
    numInfo = tonumber(stringTime)
    if numInfo < 13 then
      if numInfo < 2 then
        resultTime = 'Mid night'
      elseif numInfo < 5 then
        resultTime = 'Late night'
      elseif numInfo < 7 then
        resultTime = 'Early morning'
      elseif numInfo < 11 then
        resultTime = 'Late morning'
      else 
        resultTime = 'Mid day'
      end
    else
      if numInfo < 15 then
        resultTime = 'Early afternoon'
      elseif numInfo < 18 then
        resultTime = 'Late afternoon'
      elseif numInfo < 20 then
        resultTime = 'Early evening'
      elseif numInfo < 22 then 
        resultTime = 'Late evening'
      else
        resultTime = 'Early night'
        end
    end
    
    -- get the require date format and combine with the words
    resultDate = calendar.formatGameTime(queryDate)
    result = resultTime..', '..resultDate
    
    -- time format in 00:00 - 24:00, combined query for time and date
  elseif formatTime:lower() == '24h' then
    result = calendar.formatGameTime('%H:%M'..', '..queryDate)
    
  else
    -- default time format 12h in 00:00 - 12:00 PM/AM, combined query for time and date
    result = calendar.formatGameTime('%I:%M %p'..', '..queryDate)
  end 
  
  return result
end

return {
  getClockContent = getClockContent,

}