Clock Extended is a test plugin which enables confiuring the dimensions, position and time format for a small ingame clock. It is very much based on the code provided by Uramer here: https://gitlab.com/uramer/openmw-lua-examples

Possible widget settings in the scripting menu:

1. Time format can be:
- '24h' which means that the in-game clock will be shown in values between 00:00 and 23:59
- '12h' which means that the in-game clock will be shown in values between 00:00 and 11:59 with an added AM and PM
- 'part', which means that the in-game clock will be shown in values such as early or late morning instead of exact hours

2. Date format can be:
- 'dm' for showing the day and month
- 'dmy' for showing the day, month and year without the era numbering
- 'dmey' for showing the day, month and year with the era additionally

3. Background
- 'yes' enables a black background, which would make sense for original UI style with borders
- 'no' disables the background, which is good for UI mods that disable UI widget borders


The other parameters are to set the location and the size of the plugin.
